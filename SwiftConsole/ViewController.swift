//
//  ViewController.swift
//  SwiftConsole
//
//  Created by Dejan Novak on 05/05/2019.
//  Copyright © 2019 Ericsson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var textView: UILabel!
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = UIColor.white
        
        textView = UILabel(frame: .zero)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.numberOfLines = 0
        view.addSubview(textView!)
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: textView!, attribute: .top, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute: .top, multiplier: 1.0, constant: 8),
            NSLayoutConstraint(item: textView!, attribute: .left, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute: .left, multiplier: 1.0, constant: 8),
             NSLayoutConstraint(item: textView!, attribute: .right, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute: .right, multiplier: 1.0, constant: -8),
              NSLayoutConstraint(item: textView!, attribute: .bottom, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute: .bottom, multiplier: 1.0, constant: -8)
            ])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}

